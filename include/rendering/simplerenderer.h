#ifndef SIMPLERENDERER_H
#define SIMPLERENDERER_H

#include <rendering/rendering.h>
#include <rendering/renderer.h>
#include <vector>

namespace monolith
{
    class SimpleRenderer : public Renderer
    {
        public:
            SimpleRenderer(const Window& window) : Renderer(window) { }
            ~SimpleRenderer() { }

            virtual void renderQueue(std::vector<Renderable> queue);
        private:
    };
}

#endif // SIMPLERENDERER_H
