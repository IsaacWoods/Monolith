#ifndef RENDERING_H_INCLUDED
#define RENDERING_H_INCLUDED

#include <rendering/window.h>
#include <rendering/shader.h>
#include <rendering/renderable.h>
#include <rendering/renderer.h>

#endif // RENDERING_H_INCLUDED
