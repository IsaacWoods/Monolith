#ifndef VERTEXARRAY_H
#define VERTEXARRAY_H

#include <vector>
#include <GL/glew.h>
#include <rendering/rendering.h>

namespace monolith
{
    class VertexArray
    {
        public:
            VertexArray();
            virtual ~VertexArray();

            void addBuffer(GLuint index, Buffer* buffer);
            void bind() const;
            void unbind() const;
        private:
            GLuint m_arrayHandle;
            std::vector<Buffer*> m_buffers;
    };
}

#endif // VERTEXARRAY_H
