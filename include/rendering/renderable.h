#ifndef RENDERABLE_H
#define RENDERABLE_H

namespace monolith
{
    class Renderable
    {
        public:
            Renderable();
            virtual ~Renderable();
        protected:
        private:
    };
}

#endif // RENDERABLE_H
