#ifndef INDEXBUFFER_H
#define INDEXBUFFER_H

#include <GL/glew.h>
#include <maths.h>

namespace monolith
{
    class IndexBuffer
    {
        public:
            IndexBuffer(GLushort* data, GLsizei numElements);
            ~IndexBuffer() {}

            void bind() const;
            void unbind() const;

            inline GLuint getNumElements()      const {     return m_numElements;   }
        private:
            GLuint m_bufferHandle;
            GLuint m_numElements;
    };
}

#endif // INDEXBUFFER_H
