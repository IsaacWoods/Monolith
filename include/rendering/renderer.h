#ifndef RENDERER_H
#define RENDERER_H

#include <rendering/rendering.h>
#include <vector>

namespace monolith
{
    class Renderer
    {
        public:
            Renderer(const Window& window);
            virtual ~Renderer();

            void clear();
            virtual void renderQueue(std::vector<Renderable> queue) = 0;
        protected:
            const Window& m_window;
    };
}

#endif // RENDERER_H
