#ifndef WINDOW_H
#define WINDOW_H

#include <string>

namespace monolith
{
    class Window
    {
        public:
            Window(int width, int height, const std::string& title);
            ~Window();

            void update();
            bool hasClosed();

            inline int getWidth()                   const       {     return m_width;         }
            inline int getHeight()                  const       {     return m_height;        }
            inline const std::string getTitle()     const       {     return m_title;         }

            inline float getAspectRatio() const
            {
                return (float) m_width / (float) m_height;
            }
        private:
            int m_width, m_height;
            std::string m_title;

            bool init();
    };
}

#endif // WINDOW_H
