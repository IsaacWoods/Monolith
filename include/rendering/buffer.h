#ifndef BUFFER_H
#define BUFFER_H

#include <GL/glew.h>
#include <maths.h>

namespace monolith
{
    class Buffer
    {
        public:
            Buffer(GLfloat* data, GLsizei numElements, GLuint numComponents);
            ~Buffer() {}

            void bind() const;
            void unbind() const;
            inline GLuint getNumComponents()    const {   return m_numComponents;   }
        private:
            GLuint m_bufferHandle;
            GLuint m_numComponents;
    };
}

#endif // BUFFER_H
