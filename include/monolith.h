#ifndef MONOLITH_H
#define MONOLITH_H

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(WIN64) || defined(_WIN64)
    #define MONOLITH_WINDOWS
#elif defined(__linux__)
    #define MONOLITH_LINUX
#elif __cplusplus >= 201103L
    #define MONOLITH_OTHER_CPP11
#endif

#include <ostream>

// Import things you need when creating the engine for convenience
#include <engineoptions.h>
#include <application.h>
#include <rendering/rendering.h>
#include <maths.h>
#include <util.h>

#define DEBUG

namespace monolith
{
    class Application;

    class Monolith
    {
        public:
            Monolith(const EngineOptions& options);
            ~Monolith();

            void start(Application* application);
            inline std::ostream& output()       const {    return *(m_options.outputStream);     }
            inline std::ostream& error()        const {    return *(m_options.errorStream);      }

            Renderer* renderer;
            inline const Window& getWindow()   const {    return m_window;                      }
        private:
            bool m_isRunning;
            Application* m_application;

            const EngineOptions& m_options;
            Window m_window;

            void cleanUp();
    };
}

#endif // MONOLITH_H
