#ifndef APPLICATION_H
#define APPLICATION_H

#include <monolith.h>

namespace monolith
{
    class Monolith;

    class Application
    {
        public:
            Application(Monolith& engine) : m_monolith(engine) {}
            virtual ~Application() {}

            virtual void init() = 0;
            virtual void update(float delta) = 0;
            virtual void render() = 0;
            virtual void cleanUp() = 0;
        protected:
            Monolith& m_monolith;
    };
}

#endif // APPLICATION_H
