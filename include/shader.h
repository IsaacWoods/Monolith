#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <GL/glew.h>

namespace monolith
{
    class Shader
    {
        public:
            Shader(const std::string& vertexPath, const std::string& fragPath);
            ~Shader();

            void bind() const;
        private:
            GLuint m_programHandle;
            const std::string m_vertPath, m_fragPath;

            void generateShader();
    };
}

#endif // SHADER_H
