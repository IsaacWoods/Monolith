#ifndef ENGINEOPTIONS_H
#define ENGINEOPTIONS_H

#include <string>
#include <ostream>
#include <iostream>

namespace monolith
{
    class EngineOptions
    {
        public:
            EngineOptions();
            ~EngineOptions() {}

            void setWindowSize(int width, int height);
            void setDesiredFrameRate(int frameRate);
            void addOutputStream(const std::ostream& stream);
            void addErrorStream(const std::ostream& stream);

            int windowWidth, windowHeight;
            std::string windowTitle;
            double frameTime;
            double profileFrequency;

            std::ostream *outputStream, *errorStream;
    };
}

#endif // ENGINEOPTIONS_H
