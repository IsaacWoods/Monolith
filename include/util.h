#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <vector>
#include <monolith.h>

namespace monolith
{
    double getTime();
    void sleep(int numMillis);
    void pauseAndConfirm();

    std::string readFile(const std::string& filePath);
    std::vector<std::string> split(const std::string& str, char deliminator);
}

#endif // UTIL_H
