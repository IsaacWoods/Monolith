#ifndef MATHS_H
#define MATHS_H

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>

namespace monolith
{
    inline float toRadians(float degrees)
    {
        return degrees * (M_PI / 180.0f);
    }

    template<typename T>
    inline T clamp(const T& a, const T& min, const T& max)
    {
        if (a < min)    return min;
        if (a > max)    return max;
                        return a;
    }

    struct vec2;
    struct vec3;
    struct mat4;
    struct Quaternion;

    struct vec2
    {
        float x, y;

        vec2() : x(0.0), y(0.0) {}
        vec2(const float& x, const float& y);

        inline vec2& add(const vec2& r);
        inline vec2& sub(const vec2& r);
        inline vec2& mul(const vec2& r);
        inline vec2& div(const vec2& r);

        inline float length() const;
        inline vec2& normalised() const;
        inline float dot(const vec2& r) const;
        inline vec2& cross(const vec2& r) const;
        inline vec2& lerp(const vec2& dest, float lerpFactor) const;

        friend vec2 operator+(vec2 left, const vec2& right);
        friend vec2 operator-(vec2 left, const vec2& right);
        friend vec2 operator*(vec2 left, const vec2& right);
        friend vec2 operator/(vec2 left, const vec2& right);
        friend vec2 operator+(vec2 left, float value);
        friend vec2 operator-(vec2 left, float value);
        friend vec2 operator*(vec2 left, float value);
        friend vec2 operator/(vec2 left, float value);

        bool operator==(const vec2& other) const;
        bool operator!=(const vec2& other) const;

        vec2& operator+=(const vec2& other);
        vec2& operator-=(const vec2& other);
        vec2& operator*=(const vec2& other);
        vec2& operator/=(const vec2& other);

        friend std::ostream& operator<<(std::ostream& stream, const vec2& vec);
    };

    struct vec3
	{
		float x, y, z;

		vec3() : x(0.0), y(0.0), z(0.0) {}
		vec3(const float& v) : x(v), y(v), z(v) {}
		vec3(const float& x, const float& y, const float& z);

		inline vec3& add(const vec3& r);
		inline vec3& sub(const vec3& r);
		inline vec3& mul(const vec3& r);
		inline vec3& div(const vec3& r);

        inline float length() const;
        inline vec3& normalised() const;
        inline float dot(const vec3& r) const;
        inline vec3& cross(const vec3& r) const;
        inline vec3& lerp(const vec3& dest, float lerpFactor) const;
        inline vec3& rotate(const vec3& axis, float angle) const;
        inline vec3& rotate(const Quaternion& q) const;

		friend vec3 operator+(vec3 left, const vec3& right);
		friend vec3 operator-(vec3 left, const vec3& right);
		friend vec3 operator*(vec3 left, const vec3& right);
		friend vec3 operator/(vec3 left, const vec3& right);
		friend vec3 operator+(vec3 left, float value);
        friend vec3 operator-(vec3 left, float value);
        friend vec3 operator*(vec3 left, float value);
        friend vec3 operator/(vec3 left, float value);

		bool operator==(const vec3& other) const;
		bool operator!=(const vec3& other) const;

		vec3& operator+=(const vec3& other);
		vec3& operator-=(const vec3& other);
		vec3& operator*=(const vec3& other);
		vec3& operator/=(const vec3& other);

		friend std::ostream& operator<<(std::ostream& stream, const vec3& vec);
	};

    struct vec4
    {
        float x, y, z, w;

		vec4() : x(0.0f), y(0.0f), z(0.0f), w(0.0f) {}
		vec4(const float& v) : x(v), y(v), z(v), w(v) {}
		vec4(const float& x, const float& y, const float& z, const float& w);

		vec4& add(const vec4& r);
		vec4& sub(const vec4& r);
		vec4& mul(const vec4& r);
		vec4& div(const vec4& r);

		friend vec4 operator+(vec4 left, const vec4& right);
		friend vec4 operator-(vec4 left, const vec4& right);
		friend vec4 operator*(vec4 left, const vec4& right);
		friend vec4 operator/(vec4 left, const vec4& right);

		bool operator==(const vec4& other) const;
		bool operator!=(const vec4& other) const;

		vec4& operator+=(const vec4& other);
		vec4& operator-=(const vec4& other);
		vec4& operator*=(const vec4& other);
		vec4& operator/=(const vec4& other);

		friend std::ostream& operator<<(std::ostream& stream, const vec4& vec);
    };

    struct Quaternion
	{
	    float x, y, z, w;

		Quaternion() : x(0.0f), y(0.0f), z(0.0f), w(1.0f) {}
		Quaternion(const float& v) : x(v), y(v), z(v), w(1.0f) {}
		Quaternion(const float& x, const float& y, const float& z, const float& w);
		Quaternion(const mat4& m);

		Quaternion& add(const Quaternion& r);
		Quaternion& sub(const Quaternion& r);
		Quaternion& mul(const Quaternion& r);
		Quaternion& div(const Quaternion& r);

        float dot(const Quaternion& r) const;
        Quaternion normalised() const;
        float length() const;
		Quaternion NLerp(const Quaternion& r, float lerpFactor, bool shortestPath) const;
		Quaternion SLerp(const Quaternion& r, float lerpFactor, bool shortestPath) const;
		Quaternion lerp(const Quaternion& r, float lerpFactor) const;
		mat4 toRotationMatrix() const;
		Quaternion conjugate() const;

		inline vec3 forward() const
		{
		    return vec3(0, 0, 1).rotate(*this);
		}

		inline vec3 back() const
		{
		    return vec3(0, 0, -1).rotate(*this);
		}

		inline vec3 up() const
		{
		    return vec3(0, 1, 0).rotate(*this);
		}

		inline vec3 down() const
		{
		    return vec3(0, -1, 0).rotate(*this);
		}

		inline vec3 right() const
		{
		    return vec3(1, 0, 0).rotate(*this);
		}

		inline vec3 left() const
		{
		    return vec3(-1, 0, 0).rotate(*this);
		}

		friend Quaternion operator+(Quaternion left, const Quaternion& right);
		friend Quaternion operator-(Quaternion left, const Quaternion& right);
		friend Quaternion operator*(Quaternion left, const Quaternion& right);
		friend Quaternion operator/(Quaternion left, const Quaternion& right);
		friend Quaternion operator+(Quaternion left, float value);
		friend Quaternion operator-(Quaternion left, float value);
		friend Quaternion operator*(Quaternion left, float value);
		friend Quaternion operator/(Quaternion left, float value);
		friend Quaternion operator*(Quaternion left, const vec3& right);

		bool operator==(const Quaternion& other) const;
		bool operator!=(const Quaternion& other) const;

		Quaternion& operator+=(const Quaternion& other);
		Quaternion& operator-=(const Quaternion& other);
		Quaternion& operator*=(const Quaternion& other);
		Quaternion& operator/=(const Quaternion& other);

		friend std::ostream& operator<<(std::ostream& stream, const Quaternion& vec);
	};

	struct mat4
	{
	    union
	    {
	        float m[4 * 4];
	        vec4 columns[4];
	    };

	    mat4();
	    mat4(float diagonal);

	    vec4 getColumn(int index)
	    {
	        index *= 4;
	        return vec4(m[index], m[index + 1], m[index + 2], m[index + 3]);
	    }

	    static mat4 identity();

	    mat4& mul(const mat4& other);
	    friend mat4 operator*(mat4 left, const mat4& right);
	    mat4& operator*=(const mat4& other);

	    vec3 mul(const vec3& vec) const;
	    friend vec3 operator*(const mat4& left, const vec3& right);

	    vec4 mul(const vec4& vec) const;
	    friend vec4 operator*(const mat4& left, const vec4& right);

        static mat4 orthographic(float left, float right, float bottom, float top, float near, float far);
	    static mat4 perspective(float fov, float aspectRatio, float near, float far);

	    static mat4 translation(const vec3& translation);
	    static mat4 rotation(const vec3& axis, float angle);
	    static mat4 rotation(const vec3& forward, const vec3& up);
	    static mat4 scale(const vec3& scale);

	    mat4& invert();
	};
}

#endif // MATHS_H
