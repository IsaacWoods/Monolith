#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <maths.h>

namespace monolith
{
    class Transform
    {
        public:
            Transform(const vec3& position = vec3(0, 0, 0), const Quaternion& rotation = Quaternion(0, 0, 0, 1), float scale = 1.0f) :
                m_position(position),
                m_rotation(rotation),
                m_scale(scale),
                m_parent(0)
            { }

            void lookAt(const vec3& point, const vec3& upDir);
            const mat4& getTransformation();

            inline vec3* position()                     { return &m_position; }
            inline const vec3& position()       const  { return m_position;  }
            inline Quaternion* rotation()               { return &m_rotation; }
            inline const Quaternion& rotation() const  { return m_rotation;  }
            inline float scale()                const  { return m_scale;     }

            inline void setPosition(const vec3& position)       { m_position = position;    }
            inline void setRotation(const Quaternion& rotation) { m_rotation = rotation;    }
            inline void setScale(float scale)                   { m_scale = scale;          }
            inline void setParent(Transform* parent)             {   m_parent = parent;      }
        private:
            bool hasChanged() const;

            Transform* m_parent;
            mat4 m_transformation;

            vec3 m_position, m_oldPosition;
            Quaternion m_rotation, m_oldRotation;
            float m_scale, m_oldScale;
    };
}

#endif // TRANSFORM_H
