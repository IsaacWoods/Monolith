#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <vector>
#include <rendering/renderer.h>
#include <maths.h>

namespace monolith
{
    class GameObject
    {
        public:
            GameObject(const vec3& position = vec3(0, 0, 0), const Quaternion& rotation = Quaternion(0, 0, 0, 1), float scale = 1.0f);
            virtual ~GameObject();

            Transform& getTransform()   const { return m_transform; }

            void update(float delta);
            void render(const Renderer& renderer) const;

            GameObject* addChild(GameObject* child);
            GameObject* addComponent(Component* component);
        private:
            std::vector<GameObject*> m_children;
            std::vector<Component*> m_components;
            Transform m_transform;

            GameObject(const GameObject& other) {}
            void operator=(const GameObject& other) {}
    };
}

#endif // GAMEOBJECT_H
