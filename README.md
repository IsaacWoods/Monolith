Monolith
========

Monolith is a game engine written in C++ that can be used for creating both 2D and 3D games with beautiful graphics and realistic physics.
Monolith's design and implementation is centred around a workflow following the user and their application, not the engine's or SDK's limitations. It has been an ongoing project since I was 14, and has slowly matured into what it is today. It is designed to be used in conjunction with the Monolith SDK (still in development) to create many different types of games, from simple 2D platformers to 3D MMORPGs, all using an intuative, content-driven workflow.

Aims
---

- Aim 1     -    Maintaining a scalable, managed architecture
- Aim 2     -    Providing a stunning but fast rendering platform, with advanced culling and spatial partitioning
- Aim 3     -    Simulating a highly realistic physics world, allowing objects and characters to realistically interact with their environment
- Aim 4     -    Providing an intuative workflow based around the game, not the engine's or SDK's limitations
- Aim 5     -    Allow the engine to be easily extended to support fork projects easily
- Aim 6     -    To maintain a codebase that can be easily studied in it's entirity or in small sections

Licencing
--------

Monolith is meant as a learning aid, but also has the potential to be used in production-level games. The licence can be found in full in the LICENCE file, but my standpoint is that the engine is remaining my intellectual property, and I'm fine with people accessing the code in order to learn from my mistakes and (few and far between) successes, but not for pure monetary gain etc.

In the future, it is possible that Monolith will be in a state that makes it suitable for creating commertially-viable games. In this case, rest assured that the engine will still remain open-source and valid as a learning-aid, but additional terms may be added to the licence, at the disgression of any future contributers and me.