#version 330 core

layout (location = 0) in vec4 position;
layout (location = 1) in vec2 uv;
layout (location = 2) in float texId;
layout (location = 3) in vec4 color;

/*uniform mat4 projMatrix;
uniform mat4 viewMatrix = mat4(1.0);
uniform mat4 modelMatrix = mat4(1.0);

out DATA
{
    vec4 position;
    vec2 uv;
    float texId;
    vec4 color;
} vs_out;

void main()
{
	gl_Position = projMatrix * viewMatrix * modelMatrix * position;
	vs_out.position = modelMatrix * position;
	vs_out.uv = uv;
	vs_out.texId = texId;
	vs_out.color = color;
}
*/

void main()
{
    gl_Position = position;
}
