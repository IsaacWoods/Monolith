#include "rendering/renderer.h"
#include <GL/glew.h>

namespace monolith
{
    Renderer::Renderer(const Window& window) :
        m_window(window)
    {
        glClearColor(1.0f, 0.0f, 1.0f, 1.0f);
    }

    Renderer::~Renderer()
    {
        //dtor
    }

    void Renderer::clear()
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
}
