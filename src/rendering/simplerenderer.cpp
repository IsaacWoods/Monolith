#include "rendering/simplerenderer.h"

namespace monolith
{
    void SimpleRenderer::renderQueue(std::vector<Renderable> queue)
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        for (auto& i : queue)
        {
            // TODO: render them
        }
    }
}
