#include "rendering/vertexarray.h"

namespace monolith
{
    VertexArray::VertexArray()
    {
        glGenVertexArrays(1, &m_arrayHandle);
    }

    VertexArray::~VertexArray()
    {
        for (int i = 0; i < m_buffers.size(); i++)
            delete m_buffers[i];
    }

    void VertexArray::addBuffer(GLuint index, Buffer* buffer)
    {
        bind();
        buffer->bind();

        //glEnableVertexArrayAttrib(index);
        //glVertexAttribPointer(index, buffer->getNumComponents(), GL_FLOAT, GL_FALSE, 0, 0);

        buffer->unbind();
        unbind();
    }

    void VertexArray::bind() const
    {
        glBindVertexArray(m_arrayHandle);
    }

    void VertexArray::unbind() const
    {
        glBindVertexArray(0);
    }
}
