#include "rendering/buffer.h"

namespace monolith
{
    Buffer::Buffer(GLfloat* data, GLsizei numElements, GLuint numComponents) :
        m_numComponents(numComponents)
    {
        glGenBuffers(1, &m_bufferHandle);
        glBindBuffer(GL_ARRAY_BUFFER, m_bufferHandle);
        glBufferData(GL_ARRAY_BUFFER, numElements * sizeof(GLfloat), data, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    void Buffer::bind() const
    {
        glBindBuffer(GL_ARRAY_BUFFER, m_bufferHandle);
    }

    void Buffer::unbind() const
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
}
