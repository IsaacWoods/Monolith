#include <rendering/shader.h>
#include <util.h>
#include <GL/glew.h>
#include <vector>
#include <iostream>
#include <fstream>

//#define DEBUG_SHADERS

namespace monolith
{
    static void checkShaderError(int shaderHandle, int flag, bool isProgram, const std::string& errorMessage);
    static std::string loadShader(const std::string& fileName);

    Shader::Shader(const std::string& vertPath, const std::string& fragPath) :
        m_vertPath(vertPath),
        m_fragPath(fragPath)
    {
        m_programHandle = glCreateProgram();
        generateShader();
    }

    Shader::~Shader()
    {
        glDeleteProgram(m_programHandle);
    }

    void Shader::bind() const
    {
        glUseProgram(m_programHandle);
    }

    void Shader::generateShader()
    {
        GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
        GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

        std::string vertexSource = loadShader(m_vertPath);
        std::string fragmentSource = loadShader(m_fragPath);

        #ifdef DEBUG_SHADERS
            std::cout << "---\n" << vertexSource << "\n--End Vertex Shader--\n" << fragmentSource << "\n---" << std::endl;
            monolith::pauseAndConfirm();
        #endif

        const char* cVertexSource = vertexSource.c_str();
        const char* cFragmentSource = fragmentSource.c_str();

        glShaderSource(vertexShader, 1, &cVertexSource, NULL);
        glCompileShader(vertexShader);
        checkShaderError(vertexShader, GL_COMPILE_STATUS, false, "Failed to compile vertex shader");

        glShaderSource(fragmentShader, 1, &cFragmentSource, NULL);
        glCompileShader(fragmentShader);
        checkShaderError(fragmentShader, GL_COMPILE_STATUS, false, "Failed to compile fragment shader");

        glAttachShader(m_programHandle, vertexShader);
        glAttachShader(m_programHandle, fragmentShader);

        glLinkProgram(m_programHandle);
        checkShaderError(m_programHandle, GL_LINK_STATUS, true, "Error linking shader program");

        glValidateProgram(m_programHandle);
        checkShaderError(m_programHandle, GL_VALIDATE_STATUS, true, "Error validating shader program");

        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
    }

    static void checkShaderError(int handle, int flag, bool isProgram, const std::string& errorMessage)
    {
        /* -- Use this code instead of doing the whole allocation of 1024 chars --
        GLint length;
            glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &length);
            std::vector<char> error(length);
            glGetShaderInfoLog(fragmentShader, length, &length, &error[0]);
        */

        GLint success = 0;
        GLchar error[1024] = { 0 };

        if (isProgram)
            glGetProgramiv(handle, flag, &success);
        else
            glGetShaderiv(handle, flag, &success);

        if (!success)
        {
            if (isProgram)
            {
                glGetProgramInfoLog(handle, sizeof(error), NULL, error);
                glDeleteProgram(handle);
            }
            else
            {
                glGetShaderInfoLog(handle, sizeof(error), NULL, error);
                glDeleteShader(handle);
            }

            fprintf(stderr, "%s: '%s'\n", errorMessage.c_str(), error);
            monolith::pauseAndConfirm();
        }
    }

    static std::string loadShader(const std::string& fileName)
    {
        std::ifstream file;
        file.open(("assets/shaders/" + fileName).c_str());

        std::string output, line;

        if (file.is_open())
        {
            while (file.good())
            {
                getline(file, line);

                if (line.find("#include") == std::string::npos)
                    output.append(line + "\n");
                else
                {
                    std::string includeFileName = monolith::split(line, ' ')[1];
                    includeFileName = includeFileName.substr(1, includeFileName.length() - 2);

                    std::string included = loadShader(includeFileName);
                    output.append(included + "\n");
                }
            }
        }
        else
        {
            std::cerr << "Unable to load a shader from file: " << fileName << "!" << std::endl;
            monolith::pauseAndConfirm();
        }

        return output;
    }
}
