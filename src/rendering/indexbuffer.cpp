#include "rendering/indexbuffer.h"

namespace monolith
{
    IndexBuffer::IndexBuffer(GLushort* data, GLsizei numElements) :
        m_numElements(numElements)
    {
        glGenBuffers(1, &m_bufferHandle);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufferHandle);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, numElements * sizeof(GLushort), data, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    void IndexBuffer::bind() const
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufferHandle);
    }

    void IndexBuffer::unbind() const
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
}
