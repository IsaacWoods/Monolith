#include "shader.h"
#include "util.h"
#include <GL/glew.h>
#include <vector>
#include <iostream>

namespace monolith
{
    Shader::Shader(const std::string& vertPath, const std::string& fragPath) :
        m_vertPath(vertPath),
        m_fragPath(fragPath)
    {
        m_programHandle = glCreateProgram();
        generateShader();
    }

    Shader::~Shader()
    { }

    void Shader::bind() const
    {
        glUseProgram(m_programHandle);
    }

    void Shader::generateShader()
    {
        GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
        GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

        const char* vertexSource = monolith::readFile(m_vertPath).c_str();
        const char* fragmentSource = monolith::readFile(m_fragPath).c_str();

        glShaderSource(vertexShader, 1, &vertexSource, NULL);
        glCompileShader(vertexShader);

        GLint result;
        glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &result);

        if (result == GL_FALSE)
        {
            GLint length;
            glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &length);
            std::vector<char> error(length);
            glGetShaderInfoLog(vertexShader, length, &length, &error[0]);

            std::cout << "Failed to compile vertex shader: " << &error[0] << std::endl;
            glDeleteShader(vertexShader);

            return;
        }

        glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
        glCompileShader(fragmentShader);

        glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &result);

        if (result == GL_FALSE)
        {
            GLint length;
            glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &length);
            std::vector<char> error(length);
            glGetShaderInfoLog(fragmentShader, length, &length, &error[0]);

            std::cout << "Failed to compile fragment shader: " << &error[0] << std::endl;
            glDeleteShader(vertexShader);
            glDeleteShader(fragmentShader);

            return;
        }

        glAttachShader(m_programHandle, vertexShader);
        glAttachShader(m_programHandle, fragmentShader);

        glLinkProgram(m_programHandle);
        glValidateProgram(m_programHandle);

        // TODO: check link and validation status

        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
    }
}
