#include "maths.h"
#include <string.h>
#include <math.h>

namespace monolith
{
    // --- Vec2 ---
    vec2::vec2(const float& x, const float& y)
    {
        this->x = x;
        this->y = y;
    }

    inline vec2& vec2::add(const vec2& r)
    {
        x += r.x;
        y += r.y;

        return *this;
    }

    inline vec2& vec2::sub(const vec2& r)
    {
        x -= r.x;
        y -= r.y;

        return *this;
    }

    inline vec2& vec2::mul(const vec2& r)
    {
        x *= r.x;
        y *= r.y;

        return *this;
    }

    inline vec2& vec2::div(const vec2& r)
    {
        x /= r.x;
        y /= r.y;

        return *this;
    }

    inline float vec2::length() const
    {
        return sqrtf(x * x + y * y);
    }

    inline vec2& vec2::normalised() const
    {
        float len = length();
        vec2 result(x / len, y / len);
        return result;
    }

    inline float vec2::dot(const vec2& r) const
    {
        return x * r.x + y * r.y;
    }

    inline vec2& vec2::cross(const vec2& r) const
    {
        vec2 result = *this / length();
        return result;
    }

    inline vec2& vec2::lerp(const vec2& dest, float lerpFactor) const
    {
        vec2 result = dest - *this * lerpFactor + *this;
        return result;
    }

    vec2 operator+(vec2 left, const vec2& right)
    {
        return left.add(right);
    }

    vec2 operator-(vec2 left, const vec2& right)
    {
        return left.sub(right);
    }

    vec2 operator*(vec2 left, const vec2& right)
    {
        return left.mul(right);
    }

    vec2 operator/(vec2 left, const vec2& right)
    {
        return left.div(right);
    }

    vec2 operator+(vec2 left, float value)
    {
        vec2 result(left.x + value, left.y + value);
        return result;
    }

    vec2 operator-(vec2 left, float value)
    {
        vec2 result(left.x - value, left.y - value);
        return result;
    }

    vec2 operator*(vec2 left, float value)
    {
        vec2 result(left.x * value, left.y * value);
        return result;
    }

    vec2 operator/(vec2 left, float value)
    {
        vec2 result(left.x / value, left.y / value);
        return result;
    }

    vec2& vec2::operator+=(const vec2& other)
    {
        return add(other);
    }

    vec2& vec2::operator-=(const vec2& other)
    {
        return sub(other);
    }

    vec2& vec2::operator*=(const vec2& other)
    {
        return mul(other);
    }

    vec2& vec2::operator/=(const vec2& other)
    {
        return div(other);
    }

    bool vec2::operator==(const vec2& other) const
    {
        return x == other.x && y == other.y;
    }

    bool vec2::operator!=(const vec2& other) const
    {
        return !(*this == other);
    }

    std::ostream& operator<<(std::ostream& stream, const vec2& vec)
    {
        stream << "vec2(" << vec.x << ", " << vec.y << ")";
        return stream;
    }

    // --- Vec3 ---
    vec3::vec3(const float& x, const float& y, const float& z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    inline vec3& vec3::add(const vec3& r)
    {
        x += r.x;
        y += r.y;
        x += r.z;

        return *this;
    }

    inline vec3& vec3::sub(const vec3& r)
    {
        x -= r.x;
        y -= r.y;
        z -= r.z;

        return *this;
    }

    inline vec3& vec3::mul(const vec3& r)
    {
        x *= r.x;
        y *= r.y;
        z *= r.z;

        return *this;
    }

    inline vec3& vec3::div(const vec3& r)
    {
        x /= r.x;
        y /= r.y;
        z /= r.z;

        return *this;
    }

    inline float vec3::length() const
    {
        return sqrtf(x * x + y * y + z * z);
    }

    inline vec3& vec3::normalised() const
    {
        float len = length();
        vec3 result(x / len, y / len, z / len);
        return result;
    }

    inline float vec3::dot(const vec3& r) const
    {
        return x * r.x + y * r.y + z * r.z;
    }

    inline vec3& vec3::cross(const vec3& r) const
    {
        float x_ = y * r.z - z * r.y;
		float y_ = z * r.x - x * r.z;
		float z_ = x * r.y - y * r.x;

        vec3 result(x_, y_, z_);
		return result;
    }

    inline vec3& vec3::lerp(const vec3& dest, float lerpFactor) const
    {
        vec3 result = dest - *this * lerpFactor + *this;
        return result;
    }

    inline vec3& vec3::rotate(const vec3& axis, float angle) const
    {
        const float sinAngle = sin(-angle);
		const float cosAngle = cos(-angle);

		vec3 result = cross(axis * sinAngle) +           //Rotation on local X
			(*this * cosAngle) +                            //Rotation on local Z
                axis * dot(axis * (1 - cosAngle));              //Rotation on local Y

        return result;
    }

    inline vec3& vec3::rotate(const Quaternion& q) const
    {
        Quaternion w = q * (*this) * q.conjugate();
        vec3 result(w.x, w.y, w.z);
        return result;
    }

    vec3 operator+(vec3 left, const vec3& right)
    {
        return left.add(right);
    }

    vec3 operator-(vec3 left, const vec3& right)
    {
        return left.sub(right);
    }

    vec3 operator*(vec3 left, const vec3& right)
    {
        return left.mul(right);
    }

    vec3 operator/(vec3 left, const vec3& right)
    {
        return left.div(right);
    }

    vec3 operator+(vec3 left, float value)
    {
        vec3 result(left.x + value, left.y + value, left.z + value);
        return result;
    }

    vec3 operator-(vec3 left, float value)
    {
        vec3 result(left.x - value, left.y - value, left.z - value);
        return result;
    }

    vec3 operator*(vec3 left, float value)
    {
        vec3 result(left.x * value, left.y * value, left.z * value);
        return result;
    }

    vec3 operator/(vec3 left, float value)
    {
        vec3 result(left.x / value, left.y / value, left.z / value);
        return result;
    }

    vec3& vec3::operator+=(const vec3& other)
    {
        return add(other);
    }

    vec3& vec3::operator-=(const vec3& other)
    {
        return sub(other);
    }

    vec3& vec3::operator*=(const vec3& other)
    {
        return mul(other);
    }

    vec3& vec3::operator/=(const vec3& other)
    {
        return div(other);
    }

    bool vec3::operator==(const vec3& other) const
    {
        return x == other.x && y == other.y && z == other.z;
    }

    bool vec3::operator!=(const vec3& other) const
    {
        return !(*this == other);
    }

    std::ostream& operator<<(std::ostream& stream, const vec3& vec)
    {
        stream << "vec3(" << vec.x << ", " << vec.y << ", " << vec.z << ")";
        return stream;
    }

    // --- Vec4 ---
    vec4::vec4(const float& x, const float& y, const float& z, const float& w)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        this->w = w;
    }

    vec4& vec4::add(const vec4& r)
    {
        x += r.x;
        y += r.y;
        z += r.z;
        w += r.w;

        return *this;
    }

    vec4& vec4::sub(const vec4& r)
    {
        x -= r.x;
        y -= r.y;
        z -= r.z;
        w -= r.w;

        return *this;
    }

    vec4& vec4::mul(const vec4& r)
    {
        x *= r.x;
        y *= r.y;
        z *= r.z;
        w *= r.w;

        return *this;
    }

    vec4& vec4::div(const vec4& r)
    {
        x /= r.x;
        y /= r.y;
        z /= r.z;
        w /= r.w;

        return *this;
    }

    vec4 operator+(vec4 left, const vec4& right)
	{
		return left.add(right);
	}

	vec4 operator-(vec4 left, const vec4& right)
	{
		return left.sub(right);
	}

	vec4 operator*(vec4 left, const vec4& right)
	{
		return left.mul(right);
	}

	vec4 operator/(vec4 left, const vec4& right)
	{
		return left.div(right);
	}

	vec4& vec4::operator+=(const vec4& other)
	{
		return add(other);
	}

	vec4& vec4::operator-=(const vec4& other)
	{
		return sub(other);
	}

	vec4& vec4::operator*=(const vec4& other)
	{
		return mul(other);
	}

	vec4& vec4::operator/=(const vec4& other)
	{
		return div(other);
	}

	bool vec4::operator==(const vec4& other) const
	{
		return x == other.x && y == other.y && z == other.z && w == other.w;
	}

	bool vec4::operator!=(const vec4& other) const
	{
		return !(*this == other);
	}

    std::ostream& operator<<(std::ostream& stream, const vec4& vec)
    {
        stream << "vec4(" << vec.x << ", " << vec.y << ", " << vec.z << ", " << vec.w << ")";
        return stream;
    }

    // --- Quaternion ---
    Quaternion::Quaternion(const float& x, const float& y, const float& z, const float& w)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        this->w = w;
    }

    /*
     * This method is taken from Ken Shoemake's "Quaternion Calculus and Fast Animation" paper
     */
    Quaternion::Quaternion(const mat4& m)
    {
        float trace = m.m[0 * 4 + 0] + m.m[1 * 4 + 1] + m.m[2 * 4 + 2];

        if (trace > 0)
        {
            float s = 0.5f / sqrtf(trace + 1.0f);
            w = 0.25f / s;
            x = (m.m[1 * 4 + 2] - m.m[2 * 4 + 1]) * s;
            y = (m.m[2 * 4 + 0] - m.m[0 * 4 + 2]) * s;
            z = (m.m[0 * 4 + 1] - m.m[1 * 4 + 0]) * s;
        }
        else
        {
            if (m.m[0 * 4 + 0] > m.m[1 * 4 + 1] && m.m[0 * 4 + 0] > m.m[2 * 4 + 2])
            {
                float s = 2.0f * sqrtf(1.0f + m.m[0 * 4 + 0] - m.m[1 * 4 + 1] - m.m[2 * 4 + 2]);
				w = (m.m[1 * 4 + 2] - m.m[2 * 4 + 1]) / s;
				x = 0.25f * s;
				y = (m.m[1 * 4 + 0] + m.m[0 * 4 + 1]) / s;
				z = (m.m[2 * 4 + 0] + m.m[0 * 4+ 2]) / s;
            }
            else if (m.m[1 * 4 + 1] > m.m[2 * 4 + 2])
            {
				float s = 2.0f * sqrtf(1.0f + m.m[1 * 4 + 1] - m.m[0 * 4 + 0] - m.m[2 * 4 + 2]);
				w = (m.m[2 * 4 + 0] - m.m[0 * 4 + 2]) / s;
				x = (m.m[1 * 4 + 0] + m.m[0 * 4 + 1]) / s;
				y = 0.25f * s;
				z = (m.m[2 * 4 + 1] + m.m[1 * 4 + 2]) / s;
			}
			else
			{
				float s = 2.0f * sqrtf(1.0f + m.m[2 * 4 + 2] - m.m[0, 0] - m.m[1 * 4 + 1]);
				w = (m.m[0 * 4 + 1] - m.m[1 * 4 + 0]) / s;
				x = (m.m[2 * 4 + 0] + m.m[0 * 4 + 2]) / s;
				y = (m.m[1 * 4 + 2] + m.m[2 * 4 + 1]) / s;
				z = 0.25f * s;
			}
        }

        float length = sqrtf(x * x + y * y + z * z + w * w);
		x /= length;
		y /= length;
		z /= length;
		w /= length;
    }

    Quaternion& Quaternion::add(const Quaternion& r)
    {
        x += r.x;
        y += r.y;
        z += r.z;
        w += r.w;

        return *this;
    }

    Quaternion& Quaternion::sub(const Quaternion& r)
    {
        x -= r.x;
        y -= r.y;
        z -= r.z;
        w -= r.w;

        return *this;
    }

    Quaternion& Quaternion::mul(const Quaternion& r)
    {
        x *= r.x;
        y *= r.y;
        z *= r.z;
        w *= r.w;

        return *this;
    }

    Quaternion& Quaternion::div(const Quaternion& r)
    {
        x /= r.x;
        y /= r.y;
        z /= r.z;
        w /= r.w;

        return *this;
    }

    float Quaternion::dot(const Quaternion& r) const
    {
        return (x * r.x + y + r.y * z + r.z * w + r.w);
    }

    Quaternion Quaternion::normalised() const
    {
        return *this / length();
    }

    float Quaternion::length() const
    {
        return sqrtf(x * x + y * y + z * z + w * w);
    }

    Quaternion Quaternion::NLerp(const Quaternion& r, float lerpFactor, bool shortestPath) const
    {
        Quaternion correctedDest;

		if(shortestPath && this->dot(r) < 0)
			correctedDest = r * -1;
		else
			correctedDest = r;

		return Quaternion(lerp(correctedDest, lerpFactor).normalised());
    }

    Quaternion Quaternion::SLerp(const Quaternion& r, float lerpFactor, bool shortestPath) const
    {
        static const float EPSILON = 1e3;

		float cos = this->dot(r);
		Quaternion correctedDest;

		if(shortestPath && cos < 0)
		{
			cos *= -1;
			correctedDest = r * -1;
		}
		else
			correctedDest = r;

		if(fabs(cos) > (1 - EPSILON))
			return NLerp(correctedDest, lerpFactor, false);

		float sin = (float) sqrtf(1.0f - cos * cos);
		float angle = atan2(sin, cos);
		float invSin = 1.0f / sin;

		float srcFactor = sinf((1.0f - lerpFactor) * angle) * invSin;
		float destFactor = sinf((lerpFactor) * angle) * invSin;

		return Quaternion((*this) * srcFactor + correctedDest * destFactor);
    }

    Quaternion Quaternion::lerp(const Quaternion& r, float lerpFactor) const
    {
        return (r - *this) * lerpFactor + *this;
    }

    mat4 Quaternion::toRotationMatrix() const
    {
        vec3 forward(   2.0f * (x * z - w * y),             2.0f * (y * z + w * x),             1.0f - 2.0f * (x * x + y * y));
		vec3 up(        2.0f * (x * y + w * z),             1.0f - 2.0f * (x * x + z * z),      2.0f * (y * z - w * x));

		return mat4::rotation(forward, up);
    }

    Quaternion Quaternion::conjugate() const
    {
        return Quaternion(-x, -y, -z, w);
    }

    Quaternion operator+(Quaternion left, const Quaternion& right)
	{
		return left.add(right);
	}

	Quaternion operator-(Quaternion left, const Quaternion& right)
	{
		return left.sub(right);
	}

	Quaternion operator*(Quaternion left, const Quaternion& right)
	{
		return left.mul(right);
	}

	Quaternion operator/(Quaternion left, const Quaternion& right)
	{
		return left.div(right);
	}

    Quaternion operator+(Quaternion left, float value)
    {
        Quaternion result(left.x + value, left.y + value, left.x + value, left.w + value);
        return result;
    }

    Quaternion operator-(Quaternion left, float value)
    {
        Quaternion result(left.x - value, left.y - value, left.x - value, left.w - value);
        return result;
    }

    Quaternion operator*(Quaternion left, float value)
    {
        Quaternion result(left.x * value, left.y * value, left.x * value, left.w * value);
        return result;
    }

    Quaternion operator/(Quaternion left, float value)
    {
        Quaternion result(left.x / value, left.y / value, left.x / value, left.w / value);
        return result;
    }

    Quaternion operator*(Quaternion left, const vec3& right)
    {
        const float _w = - (left.x * right.x) - (left.y * right.y) - (left.z * right.z);
		const float _x =   (left.w * right.x) + (left.y * right.z) - (left.z * right.y);
		const float _y =   (left.w * right.y) + (left.z * right.x) - (left.x * right.z);
		const float _z =   (left.w * right.z) + (left.x * right.y) - (left.y * right.x);

		return Quaternion(_x, _y, _z, _w);
    }

	Quaternion& Quaternion::operator+=(const Quaternion& other)
	{
		return add(other);
	}

	Quaternion& Quaternion::operator-=(const Quaternion& other)
	{
		return sub(other);
	}

	Quaternion& Quaternion::operator*=(const Quaternion& other)
	{
		return mul(other);
	}

	Quaternion& Quaternion::operator/=(const Quaternion& other)
	{
		return div(other);
	}

	bool Quaternion::operator==(const Quaternion& other) const
	{
		return x == other.x && y == other.y && z == other.z && w == other.w;
	}

	bool Quaternion::operator!=(const Quaternion& other) const
	{
		return !(*this == other);
	}

    std::ostream& operator<<(std::ostream& stream, const Quaternion& vec)
    {
        stream << "Quaternion(" << vec.x << ", " << vec.y << ", " << vec.z << ", " << vec.w << ")";
        return stream;
    }

    // --- Mat4 ---
    mat4::mat4()
    {
        for (int i = 0; i < 4 * 4; i++)
            m[i] = 0.0;
    }

    mat4::mat4(float diagonal)
    {
        for (int i = 0; i < 4 * 4; i++)
            m[i] = 0.0;

        m[0 + 0 * 4] = diagonal;
        m[1 + 1 * 4] = diagonal;
        m[2 + 2 * 4] = diagonal;
        m[3 + 3 * 4] = diagonal;
    }

    mat4 mat4::identity()
    {
        return mat4(1.0f);
    }

    mat4& mat4::mul(const mat4& other)
    {
        float data[16];

		for (int y = 0; y < 4; y++)
			for (int x = 0; x < 4; x++)
			{
				float sum = 0.0f;

				for (int e = 0; e < 4; e++)
					sum += m[x + e * 4] * other.m[e + y * 4];

				data[x + y * 4] = sum;
			}

		memcpy(m, data, 4 * 4 * sizeof(float));

		return *this;
    }

    vec3 mat4::mul(const vec3& other) const
	{
		return vec3(
			columns[0].x * other.x + columns[1].x * other.y + columns[2].x * other.z + columns[3].x,
			columns[0].y * other.x + columns[1].y * other.y + columns[2].y * other.z + columns[3].y,
			columns[0].z * other.x + columns[1].z * other.y + columns[2].z * other.z + columns[3].z
		);
	}

	vec4 mat4::mul(const vec4& other) const
	{
		return vec4(
			columns[0].x * other.x + columns[1].x * other.y + columns[2].x * other.z + columns[3].x * other.w,
			columns[0].y * other.x + columns[1].y * other.y + columns[2].y * other.z + columns[3].y * other.w,
			columns[0].z * other.x + columns[1].z * other.y + columns[2].z * other.z + columns[3].z * other.w,
			columns[0].w * other.x + columns[1].w * other.y + columns[2].w * other.z + columns[3].w * other.w
		);
	}

	mat4& mat4::operator*=(const mat4& other)
	{
		return mul(other);
	}

    mat4 operator*(mat4 left, const mat4& right)
	{
		return left.mul(right);
	}

	vec3 operator*(const mat4& left, const vec3& right)
	{
		return left.mul(right);
	}

	vec4 operator*(const mat4& left, const vec4& right)
	{
		return left.mul(right);
	}

	mat4 mat4::orthographic(float left, float right, float bottom, float top, float near, float far)
	{
	    mat4 result(1.0f);

	    result.m[0 + 0 * 4] = 2.0f / (right - left);
	    result.m[1 + 1 * 4] = 2.0f / (top - bottom);
	    result.m[2 + 2 * 4] = 2.0f / (near - far);

	    result.m[0 + 3 * 4] = (left + right) / (left - right);
	    result.m[1 + 3 * 4] = (bottom + top) / (bottom - top);
	    result.m[2 + 3 * 4] = (near + far) / (far - near);

	    return result;
	}

	mat4 mat4::perspective(float fov, float aspectRatio, float near, float far)
	{
	    mat4 result(1.0f);
	    float q = 1.0f / tan(0.5f * fov);

	    result.m[0 + 0 * 4] = q / aspectRatio;
	    result.m[1 + 1 * 4] = q;
	    result.m[2 + 2 * 4] = (near + far) / (near - far);
	    result.m[3 + 3 * 4] = -1.0f;
	    result.m[2 + 3 * 4] = (2.0f * near * far) / (near - far);

	    return result;
	}

    mat4 mat4::translation(const vec3& translation)
    {
        mat4 result(1.0f);

        result.m[0 + 3 * 4] = translation.x;
        result.m[1 + 3 * 4] = translation.y;
        result.m[2 + 3 * 4] = translation.z;

        return result;
    }

	mat4 mat4::rotation(const vec3& axis, float angle)
	{
	    mat4 result(1.0f);

	    float rotCos = cos(angle);
	    float rotSin = sin(angle);
	    float omCos = 1.0f - rotCos;

	    float x = axis.x;
	    float y = axis.y;
	    float z = axis.z;

	    result.m[0 + 0 * 4] = x * omCos + rotCos;
		result.m[1 + 0 * 4] = y * x * omCos + z * rotSin;
		result.m[2 + 0 * 4] = x * z * omCos - y * rotSin;

		result.m[0 + 1 * 4] = x * y * omCos - z * rotSin;
		result.m[1 + 1 * 4] = y * omCos + rotCos;
		result.m[2 + 1 * 4] = y * z * omCos + x * rotSin;

		result.m[0 + 2 * 4] = x * z * omCos + y * rotSin;
		result.m[1 + 2 * 4] = y * z * omCos - x * rotSin;
		result.m[2 + 2 * 4] = z * omCos + rotCos;

	    return result;
	}

    mat4 mat4::rotation(const vec3& forward, const vec3& up)
    {
        mat4 result;

        vec3 n = forward.normalised();
        vec3 u = up.normalised().cross(n);
        vec3 v = n.cross(u);

        result.m[0 * 4 + 0] = u.x;     result.m[1 * 4 + 0] = u.y;     result.m[2 * 4 + 0] = u.z;     result.m[3 * 4 + 0] = 0.0f;
		result.m[0 * 4 + 1] = v.x;     result.m[1 * 4 + 1] = v.y;     result.m[2 * 4 + 1] = v.z;     result.m[3 * 4 + 1] = 0.0f;
		result.m[0 * 4 + 3] = 0.0f;    result.m[1 * 4 + 3] = 0.0f;    result.m[2 * 4 + 3] = 0.0f;    result.m[3 * 4 + 3] = 1.0f;
		result.m[0 * 4 + 2] = n.x;     result.m[1 * 4 + 2] = n.y;     result.m[2 * 4 + 2] = n.z;     result.m[3 * 4 + 2] = 0.0f;

		return result;
    }

    mat4 mat4::scale(const vec3& scale)
    {
        mat4 result;

        result.m[0 + 0 * 4] = scale.x;
        result.m[1 + 1 * 4] = scale.y;
        result.m[2 + 2 * 4] = scale.z;
        result.m[3 * 3 * 4] = 1.0f;

        return result;
    }

	mat4& mat4::invert()
	{
	    double temp[16];

	    temp[0] = m[5] * m[10] * m[15] -
			m[5] * m[11] * m[14] -
			m[9] * m[6] * m[15] +
			m[9] * m[7] * m[14] +
			m[13] * m[6] * m[11] -
			m[13] * m[7] * m[10];

		temp[4] = -m[4] * m[10] * m[15] +
			m[4] * m[11] * m[14] +
			m[8] * m[6] * m[15] -
			m[8] * m[7] * m[14] -
			m[12] * m[6] * m[11] +
			m[12] * m[7] * m[10];

		temp[8] = m[4] * m[9] * m[15] -
			m[4] * m[11] * m[13] -
			m[8] * m[5] * m[15] +
			m[8] * m[7] * m[13] +
			m[12] * m[5] * m[11] -
			m[12] * m[7] * m[9];

		temp[12] = -m[4] * m[9] * m[14] +
			m[4] * m[10] * m[13] +
			m[8] * m[5] * m[14] -
			m[8] * m[6] * m[13] -
			m[12] * m[5] * m[10] +
			m[12] * m[6] * m[9];

		temp[1] = -m[1] * m[10] * m[15] +
			m[1] * m[11] * m[14] +
			m[9] * m[2] * m[15] -
			m[9] * m[3] * m[14] -
			m[13] * m[2] * m[11] +
			m[13] * m[3] * m[10];

		temp[5] = m[0] * m[10] * m[15] -
			m[0] * m[11] * m[14] -
			m[8] * m[2] * m[15] +
			m[8] * m[3] * m[14] +
			m[12] * m[2] * m[11] -
			m[12] * m[3] * m[10];

		temp[9] = -m[0] * m[9] * m[15] +
			m[0] * m[11] * m[13] +
			m[8] * m[1] * m[15] -
			m[8] * m[3] * m[13] -
			m[12] * m[1] * m[11] +
			m[12] * m[3] * m[9];

		temp[13] = m[0] * m[9] * m[14] -
			m[0] * m[10] * m[13] -
			m[8] * m[1] * m[14] +
			m[8] * m[2] * m[13] +
			m[12] * m[1] * m[10] -
			m[12] * m[2] * m[9];

		temp[2] = m[1] * m[6] * m[15] -
			m[1] * m[7] * m[14] -
			m[5] * m[2] * m[15] +
			m[5] * m[3] * m[14] +
			m[13] * m[2] * m[7] -
			m[13] * m[3] * m[6];

		temp[6] = -m[0] * m[6] * m[15] +
			m[0] * m[7] * m[14] +
			m[4] * m[2] * m[15] -
			m[4] * m[3] * m[14] -
			m[12] * m[2] * m[7] +
			m[12] * m[3] * m[6];

		temp[10] = m[0] * m[5] * m[15] -
			m[0] * m[7] * m[13] -
			m[4] * m[1] * m[15] +
			m[4] * m[3] * m[13] +
			m[12] * m[1] * m[7] -
			m[12] * m[3] * m[5];

		temp[14] = -m[0] * m[5] * m[14] +
			m[0] * m[6] * m[13] +
			m[4] * m[1] * m[14] -
			m[4] * m[2] * m[13] -
			m[12] * m[1] * m[6] +
			m[12] * m[2] * m[5];

		temp[3] = -m[1] * m[6] * m[11] +
			m[1] * m[7] * m[10] +
			m[5] * m[2] * m[11] -
			m[5] * m[3] * m[10] -
			m[9] * m[2] * m[7] +
			m[9] * m[3] * m[6];

		temp[7] = m[0] * m[6] * m[11] -
			m[0] * m[7] * m[10] -
			m[4] * m[2] * m[11] +
			m[4] * m[3] * m[10] +
			m[8] * m[2] * m[7] -
			m[8] * m[3] * m[6];

		temp[11] = -m[0] * m[5] * m[11] +
			m[0] * m[7] * m[9] +
			m[4] * m[1] * m[11] -
			m[4] * m[3] * m[9] -
			m[8] * m[1] * m[7] +
			m[8] * m[3] * m[5];

		temp[15] = m[0] * m[5] * m[10] -
			m[0] * m[6] * m[9] -
			m[4] * m[1] * m[10] +
			m[4] * m[2] * m[9] +
			m[8] * m[1] * m[6] -
			m[8] * m[2] * m[5];

		double determinant = m[0] * temp[0] + m[1] * temp[4] + m[2] * temp[8] + m[3] * temp[12];
		determinant = 1.0 / determinant;

		for (int i = 0; i < 4 * 4; i++)
			m[i] = temp[i] * determinant;

		return *this;
	}
}
