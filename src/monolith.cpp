#include "monolith.h"
#include <GL/glfw.h>

namespace monolith
{
    Monolith::Monolith(const EngineOptions& options) :
        m_isRunning(false),
        m_options(options),
        m_window(m_options.windowWidth, m_options.windowHeight, m_options.windowTitle)
    { }

    Monolith::~Monolith()
    { }

    void Monolith::start(Application* application)
    {
        if (m_isRunning)
            error() << "Attempted to start the engine when it was already running!" << std::endl;

        application->init();

        m_isRunning = true;
        m_application = application;

        double lastTime = monolith::getTime();
        double frameCounter = 0, unprocessedTime = 0;
        int frames = 0;

        while (m_isRunning)
        {
            bool isNeedRender = false;

            double startTime = monolith::getTime();
            double elapsedTime = startTime - lastTime;
            lastTime = startTime;

            unprocessedTime += elapsedTime;
            frameCounter += elapsedTime;

            if (frameCounter >= m_options.profileFrequency)
            {
                // TODO: profile the engine here - run all of the profiler tasks
                double totalTime = ((1000.0 * frameCounter) / ((double) frames));

                std::cout << "Total time: \t\t" << totalTime << " ms" << std::endl;
                frames = 0;
                frameCounter = 0;
            }

            while (unprocessedTime > m_options.frameTime)
            {
                m_window.update();

                if (m_window.hasClosed())
                    cleanUp();

                m_application->update((float) m_options.frameTime);

                #ifdef DEBUG
                    GLenum error = glGetError();
                    if (error != GL_NO_ERROR)   output() << "Encountered OpenGL error: " << error << std::endl;
                    monolith::pauseAndConfirm();
                #endif

                isNeedRender = true;
                unprocessedTime -= m_options.frameTime;
            }

            if (isNeedRender)
            {
                m_application->render();

                frames++;
            }
            else
            {
                monolith::sleep(1);
            }
        }
    }

    void Monolith::cleanUp()
    {
        m_isRunning = false;

        m_application->cleanUp();
        delete renderer;

        glfwTerminate();
    }
}
