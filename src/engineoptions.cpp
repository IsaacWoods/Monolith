#include "engineoptions.h"
#include <iostream>

namespace monolith
{
    EngineOptions::EngineOptions() :
        windowWidth(800),
        windowHeight(600),
        windowTitle("Monolith"),
        frameTime(1.0 / 60.0),
        profileFrequency(1.5),
        outputStream(&std::cout),
        errorStream(&std::cerr)
    { }

    void EngineOptions::setWindowSize(int width, int height)
    {
        windowWidth = width;
        windowHeight = height;
    }

    void EngineOptions::setDesiredFrameRate(int frameRate)
    {
        frameTime = 1.0 / (double) frameRate;
    }

    void EngineOptions::addOutputStream(const std::ostream& stream)
    {
        outputStream = outputStream;
    }

    void EngineOptions::addErrorStream(const std::ostream& stream)
    {
        errorStream = errorStream;
    }
}
