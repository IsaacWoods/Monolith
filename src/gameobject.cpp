#include "gameobject.h"

namespace monolith
{
    GameObject::GameObject(const vec3& position = vec3(0, 0, 0), const Quaternion& rotation = Quaternion(0, 0, 0, 1), float scale = 1.0f) :
        m_transform(position, rotation, scale)
    { }

    GameObject::~GameObject()
    {
        for (unsigned int i = 0; i < m_components.size(); i++)
            if (m_components[i])
                delete m_components[i];

        for (unsigned int i = 0; i < m_children.size(); i++)
            if (m_children[i])
                delete m_children[i];
    }

    GameObject* addChild(GameObject* child)
    {
        m_children.push_back(child);
        child->getTransform().setParent(m_transform);

        return this;
    }
}
