#include "util.h"
#include <chrono>
#include <unistd.h>
#include <thread>
#include <fstream>
#include <cstring>

#define UTIL_PAUSE_ON_FAIL 1

#ifdef MONOLITH_WINDOWS
    #include <Windows.h>
    #include <iostream>
    static double g_freq;
    static bool g_timerInitialized = false;
#endif

#ifdef MONOLITH_LINUX
    #include <sys/time.h>
    static const long NANOSECONDS_PER_SECOND = 1000000000l;
#endif

#ifdef MONOLITH_OTHER_CPP11
    static std::chrono::system_clock::time_point g_epoch = std::chrono::high_resolution_clock::now();
#endif

namespace monolith
{
    double getTime()
    {
        #ifdef MONOLTH_WINDOWS
            if (!g_timerInitialized)
            {
                LARGE_INTEGER li;

                // TODO: somehow get the error logger from the Monolith instance
                if (!QueryPerformanceFrequency(&li))
                    std::cerr << "Could not initialize timer on Windows!" << std::endl;

                    g_freq = double(li.QuadPart);
                    g_timerInitialized = true;
            }

            LARGE_INTEGER li;

            if (!QueryPerformaceCounter(&li))
                std::cerr << "Could not get time on Windows!" << std::endl;

            return double(li.QuadPart) / g_freq;
        #endif

        #ifdef MONOLITH_LINUX
            timespec spec;
            clock_gettime(CLOCK_REALTIME, &spec);
            return (double) (((long) spec.tv_sec * NANOSECONDS_PER_SECOND) + spec.tv_nsec) / ((double) (NANOSECONDS_PER_SECOND));
        #endif

        #ifdef MONOLITH_OTHER_CPP11
            return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - g_epoch).count() / 1000000000d;
        #endif

        return -1;
    }

    void sleep(int numMillis)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(numMillis));
    }

    std::string readFile(const std::string& filePath)
    {
        if (access(filePath.c_str(), R_OK) == -1)
        {
            std::cerr << "Could not locate file: " << filePath << std::endl;
            pauseAndConfirm();

            return NULL;
        }

        FILE* file = fopen(filePath.c_str(), "rt");
        fseek(file, 0, SEEK_END);
        unsigned int long length = ftell(file);

        char* data = new char[length + 1];
        memset(data, 0, length + 1);
        fseek(file, 0, SEEK_SET);
        fread(data, 1, length, file);

        std::string result(data);
        delete[] data;
        fclose(file);

        return result;
    }

    void pauseAndConfirm()
    {
        #ifdef UTIL_PAUSE_ON_FAIL
            std::cin.get();
        #endif
    }

    std::vector<std::string> split(const std::string& str, char deliminator)
    {
        std::vector<std::string> elems;

        const char* cstr = str.c_str();
        unsigned int length = (unsigned int) str.length();
        unsigned int start = 0, end = 0;

        while (end <= length)
        {
            while (end <= length)
            {
                if (cstr[end] == deliminator)
                    break;

                end++;
            }

            elems.push_back(str.substr(start, end - start));
            start = end - 1;
            end = start;
        }

        return elems;
    }
}
