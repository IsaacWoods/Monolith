#include <window.h>
#include <iostream>
#include <GL/glew.h>
#include <GL/glfw.h>

namespace monolith
{
    Window::Window(int width, int height, const std::string& title) :
        m_width(width),
        m_height(height),
        m_title(title)
    {
        if (!init())
            glfwTerminate();
    }

    Window::~Window()
    {
        glfwTerminate();
    }

    bool Window::init()
    {
        if (!glfwInit())
        {
            std::cerr << "Failed to initialise GLFW!" << std::endl;
            return false;
        }

        if (!glfwOpenWindow(m_width, m_height, 0, 0, 0, 0, 0, 0, GLFW_WINDOW))
        {
            std::cerr << "Failed to create GLFW window!" << std::endl;
            return false;
        }

        glfwSetWindowTitle(m_title.c_str());
        //glfwMakeContextCurrent(m_window);
		//glfwSetWindowUserPointer(m_window, this);
		//glfwSetFramebufferSizeCallback(m_window, window_resize);
		//glfwSetKeyCallback(m_window, key_callback);
		//glfwSetMouseButtonCallback(m_window, mouse_button_callback);
		//glfwSetCursorPosCallback(m_window, cursor_position_callback);
		glfwSwapInterval(0);

		if (glewInit() != GLEW_OK)
		{
		    std::cerr << "Could not initialise GLEW!" << std::endl;
		    return false;
		}

		std::cout << "OpenGL " << glGetString(GL_VERSION) << std::endl;
		return true;
    }

    void Window::update()
    {
        glfwPollEvents();
        glfwSwapBuffers();
    }

    bool Window::hasClosed()
    {
        return !glfwGetWindowParam(GLFW_OPENED);
    }
}
