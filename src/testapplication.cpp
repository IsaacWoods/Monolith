#include <iostream>
#include "monolith.h"

using namespace monolith;

class TestApplication : public Application
{
    public:
        TestApplication(Monolith& engine) :
            Application(engine)
        { }
    private:
        virtual void init();
        virtual void update(float delta);
        virtual void render();
        virtual void cleanUp();
        void operator=(const TestApplication& otherApplication) {}
};

void TestApplication::init()
{
    m_monolith.output() << "Hello, World!" << std::endl;

}

void TestApplication::update(float delta)
{

}

void TestApplication::render()
{

}

void TestApplication::cleanUp()
{

}

int main()
{
    EngineOptions options;
    options.profileFrequency = 2;

    Monolith engine(options);
    TestApplication application(engine);
    engine.start(&application);

    return 0;
}
