#include "application.h"

namespace monolith
{
    Application::Application(Monolith& engine) :
        m_monolith(engine)
    { }

    Application::~Application()
    { }

    void Application::update(float delta)
    {

    }

    void Application::render()
    {

    }
}
