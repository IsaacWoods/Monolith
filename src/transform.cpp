#include "transform.h"

namespace monolith
{
    const mat4& Transform::getTransformation()
    {
        if (hasChanged())
        {
            mat4 translationMat = mat4::translation(m_position);
            mat4 scaleMat = mat4::scale(vec3(m_scale));
            mat4 rotationMat = m_rotation.toRotationMatrix();
            mat4 transformation = (*m_parent).getTransformation() * translationMat * rotationMat * scaleMat;
            m_transformation = transformation;
        }

        return m_transformation;
    }

    bool Transform::hasChanged() const
    {
        if (m_parent != 0 && m_parent->hasChanged())    return true;
            if (m_position != m_oldPosition)                return true;
                if (m_rotation != m_oldRotation)                return true;
                    if (m_scale != m_oldScale)                      return true;
                                                                        return false;
    }

    void Transform::lookAt(const vec3& point, const vec3& upDir)
    {
        m_rotation = Quaternion(mat4::rotation((point - m_position).normalised(), upDir));
    }
}
